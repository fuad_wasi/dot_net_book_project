﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Book.Data
{
    public class BaseEntity
    {
        public int  ID { get; set; }
        public DateTime AddedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string IpAddress { get; set; }
    }
}
