﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Book.Data
{
    public class Book: BaseEntity
    {
        public string Title { get; set; }
        public int TotalPage { get; set; }
        public virtual WrittenBook WrittenBook { get; set; }

    }
}
