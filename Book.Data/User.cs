﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Book.Data
{
    public class User: BaseEntity
    {
        //public int ID { get; set; }
        public string userName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public virtual UserProfile UserProfile { get; set; }
    }
}
