﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Book.Data
{
    public class WrittenBook: BaseEntity
    {
        public virtual Author Author{ get; set; }
        public virtual Book Book{ get; set; }
    }
}
