﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Book.Data
{
    public class Author: BaseEntity
    {
        public string Name { get; set; }
        public string DateofBirth { get; set; }

        public virtual WrittenBook WrittenBook { get; set; }
    }
}
